package com.example.restaurants.domain;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document
public class Restaurant {

    @Id
    private ObjectId _id;

    private String restaurant_id;

    private Address address;

    private String borough;

    private String cuisine;

    private List<Grade> grades;

    private String name;

    private class Address {
        private String building;
        private List coord;
        private String street;
        private String zipcode;
    }

    private class Grade {
        private Date date;
        private String grade;
        private int score;
    }
}
