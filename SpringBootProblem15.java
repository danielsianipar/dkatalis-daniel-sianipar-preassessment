import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.TimeZone;

public class SpringBootProblem15 {

    //type your code here

    public static void main(String []args) {
        String date1 = "2018-09-25T02:10:24+07:00";
        String date2 = "2019-08-25T02:10:24+07:00";

        System.out.println("fromISO8601UTC(date1) = " + fromISO8601UTC(date1));
        System.out.println("fromISO8601UTC(date2) = " + fromISO8601UTC(date2));
        Duration duration = Duration.between(fromISO8601UTC(date1).toInstant(), fromISO8601UTC(date2).toInstant());
        System.out.println("duration.getSeconds() = " + duration.getSeconds());
    }

    public static Date fromISO8601UTC(String dateStr) {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
        df.setTimeZone(tz);
        try {
            return df.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return null;
    }
}
