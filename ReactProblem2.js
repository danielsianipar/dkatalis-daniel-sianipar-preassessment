const TodoItem = (props) => <li onClick={props.onClick}>{props.item.text}</li>

class TodoList extends React.Component {
  render() {
    const {items, onClick} = this.props;
    return (<ul onClick={onClick}>
      {items.map((item, index) =>
          <TodoItem item={item} key={index}
                    onClick={this.handleItemClick.bind(this, item)}/>)}
    </ul>);
  }

  handleItemClick(item, event) {
    // change code below this line
    if(false == item.done){
        item.done.onItemClick;
    }
    // change code above this line
  }
}

const items = [{text: 'Buy grocery', done: true},
  {text: 'Play guitar', done: false},
  {text: 'Romantic dinner', done: false}
];

const App = (props) => <TodoList
    items={props.items}
    onItemClick={(item, event) => {
      console.log(item, event)
    }}
/>;

document.body.innerHTML = "<div id='root'></div>";
const rootElement = document.getElementById("root");
ReactDOM.render(<App items={items}/>, rootElement);